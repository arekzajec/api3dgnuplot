0. Paczka zawiera API do rysowania brył i łamanych w 3D za pomocą gnuplota.

1. Pliki/Moduły API:
Draw3D_api_interface.hh -- klasa ujednolicająca interfejs rysowania
Dr3D_gnuplot_api.hh/cpp -- klasa realizująca powyższy interfejs rysowania w gnuplocie

2. Pliki przykładów:
ex_gnuplot.cpp -- przykład rysowania w gnuplocie

3. Plik Makefile uruchamiający przykład.

4. Wymagania:
4.1. Gnuplot
(dla ubuntu [LTS 20.04])
sudo apt install gnuplot
